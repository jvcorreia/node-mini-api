function esperarPor(tempo) {
    return new Promise(function(resolve) {
        setTimeout(function(){
            resolve()
        }, tempo)
    })
}

/*esperarPor(2000)
    .then(() => console.log('primeiro'))
    .then(esperarPor(3000))
    .then(() => console.log('segundo'))
    .then(esperarPor(1000))*/

async function exe() {
    await esperarPor(2000);
    console.log('async')
    await esperarPor(2000);
    console.log('async')
    await esperarPor(2000);
    console.log('async')
}

exe()