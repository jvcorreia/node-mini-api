function gerarNumerosEntre(min, max, numerosProibidos) {
    if (min>max) {
        let a = [max, min]
    }

    return new Promise((resolve, reject) => {
        const random = parseInt(Math.random() * (max - min + 1) + min)
        if(numerosProibidos.includes(random)){
            reject('numero repetido')
        }
        resolve(random)
    })
}

async function gerarMegaSena(qtdNumeros) {
    try{
    const numeros = []
    for(let _ of Array(qtdNumeros).fill()){
        numeros.push(await gerarNumerosEntre(1, 60, numeros))
    }
    return numeros;
    }catch(e){
        console.log(e)
    }
}


//gerarNumerosEntre(1, 5, [1, 2, 4]).catch(e => console.log(e))
gerarMegaSena(8).then(a => console.log(a))
    .catch(console.log)