function funcionarOuNao(valor, chanceErro) {
    return new Promise((resolve, reject) => {
        if (Math.random() < chanceErro) {
            reject('ocorreu erro')//retorno em caso de erro
        }else{
            resolve(valor)//retorno em caso de sucesso
        }
    })

    
}
funcionarOuNao('2', 0.7).then(valor => console.log(valor))
        .catch(e => console.log(e))