let p = new Promise(function(cumprirPromessa){
    cumprirPromessa(3)
})


p.then(function(a){ //then recebe uma função q tem como parâmetro o valor que sera 
    console.log(a)  //recebido da promise
})

//Caso queira passar mais de um valor

let x = new Promise(function(printar){
    printar({
        a:"3",
        b:"2"
    })
}).then(a => a.a)
  .then(a =>console.log(a))