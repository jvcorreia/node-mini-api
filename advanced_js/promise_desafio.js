const fs = require('fs');
const { resolve } = require('path');
const path = require('path')

const caminho = path.join(__dirname, 'man.json')

function lerArquivo(caminho) {
    return new Promise(resolve => {
        fs.readFile(caminho, function(e, content){
            resolve(JSON.parse(content)) //Resolve seria o 'retorno"
        })
    })
}

lerArquivo(caminho).then(a => console.log(a))
