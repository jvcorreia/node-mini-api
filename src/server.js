const express = require('express')

const porta = 3003

//Express precisa ser instânciado

const app = express()

const bodyParser = require('body-parser')

app.use(bodyParser.urlencoded({extended: true})) 
/*
A linha acima se trata de um middleware, onde cada requisição entrará dentro dele
Tratar as requisições (transformar em obj)
*/

const database = require('./database')

app.get('/produtos', (req, res, next) => {
    res.send(database.getProducts())
})

app.get('/produto/delete/:id', (req, res, next) => {
    res.send(database.deleteProduct(req.params.id))
})


app.post('/produto/novo', (req, res, next) => {
    const produto = database.saveProduct({
        name: req.body.name,
        price: req.body.price
    })
    res.send(produto) //json
})

app.get('/produtos/:id', (req, res, next) => {
    res.send(database.getProduct(req.params.id))
})

app.listen(porta, () => {
    console.log('server funcionando')
})