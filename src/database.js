const sequence = {
    _id : 1,
    get_id() {
        return this._id++;
    }
}

const products = {

}

function saveProduct(product) {
    if (!product.id) {
        product.id = sequence.get_id()
        products[product.id] = product
    }
    return product
}

function getProduct(product) {
    return products[product] || {msg:"erro"}
}

function deleteProduct(id) {
    delete products[id]
    return id
}

function getProducts() {
    return Object.values(products)
}

module.exports = {saveProduct, getProduct, getProducts, deleteProduct}